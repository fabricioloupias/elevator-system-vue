import { Elevator } from './Elevator';

export interface ElevatorSystem {
  elevators: Elevator[];
  totalElevators: number;
  maxFloors: number;
  nameSpace: string;
  totalFloorsByRequest: number;
}
