import { ElevatorDirection } from './ElevatorDirection';

export interface Elevator {
  id: number;
  currentFloor: number;
  direction: ElevatorDirection;
  totalFloors: number;
  downDestinationReversed: number[];
  countFloors: number;
}
