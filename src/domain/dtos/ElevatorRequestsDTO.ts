export interface PickUpDTO {
  requestedFloor: number;
  floorDestination: number;
}
export interface ElevatorRequestsDTO {
  elevatorRequests: PickUpDTO[];
}
