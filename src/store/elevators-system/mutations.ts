import { stat } from 'fs';
import { ElevatorSystem } from 'src/domain/ElevatorSystem';
import { MutationTree } from 'vuex';
import { ElevatorSystemState, initialIntervals } from './state';

const mutation: MutationTree<ElevatorSystemState> = {
  setElevatorSystem(state, payload: ElevatorSystem) {
    state.elevatorSystem = payload;
  },
  setIsLoading(state, payload: boolean) {
    state.isLoading = payload;
  },
  setInterval(state, payload: { position: number; value: number }) {
    state.countdownIntervals.splice(payload.position, 1, payload.value);
  },
  setInitialInterval(state, index: number){
    const value = initialIntervals[index]
    state.countdownIntervals.splice(index, 1, value);
  },
  setIsFirstTime(state, isFirsTime: boolean){
    state.isFirstTime = isFirsTime;
  }
};

export default mutation;
