import { ElevatorSystem } from 'src/domain/ElevatorSystem';
import { cloneDeep } from 'lodash';

export const initialIntervals = [60 * 5, 60 * 60, 60 * 600, 60 * 20, 60 * 4, 60 * 7, 60 * 7, 60 * 3];

export interface ElevatorSystemState {
  elevatorSystem: ElevatorSystem | null;
  isLoading: boolean;
  countdownIntervals: number[];
  isFirstTime: boolean;
}

const state: ElevatorSystemState = {
  elevatorSystem: null,
  isLoading: false,
  countdownIntervals: cloneDeep(initialIntervals),
  isFirstTime: true
};

export default state;
