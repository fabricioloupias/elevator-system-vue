import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { ElevatorSystemState } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const elevatorSystemModule: Module<ElevatorSystemState, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state: state
};

export default elevatorSystemModule;
