import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import axios from 'axios';
import { ElevatorSystemState } from './state';
import { ElevatorSystem } from 'src/domain/ElevatorSystem';
import { ElevatorRequestsDTO } from 'src/domain/dtos/ElevatorRequestsDTO';

const API_PATH = process.env.API || '';

const actions: ActionTree<ElevatorSystemState, StateInterface> = {
  async getElevatorSystem({ commit }) {
    try {
      const response = await axios.get<ElevatorSystem>(
        `${API_PATH}/system-elevators`
      );
      const data = response.data;

      commit('setElevatorSystem', data);
    } catch (error) {}
  },

  async pickUpElevatorAndGetElevatorSystem({ commit }, elevatorRequests: ElevatorRequestsDTO) {
    try {
      await axios.post<ElevatorSystem>(
        `${API_PATH}/system-elevators`,
        elevatorRequests
      );

      const response = await axios.get<ElevatorSystem>(
        `${API_PATH}/system-elevators`
      );
      const data = response.data;

      commit('setElevatorSystem', data);

    } catch (error) {}
  }
};

export default actions;
