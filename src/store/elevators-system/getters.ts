import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { ElevatorSystemState } from './state';

const getters: GetterTree<ElevatorSystemState, StateInterface> = {
  getElevatorSystem(state) {
    return state.elevatorSystem;
  },
  getCountdownIntervals(state){
    return state.countdownIntervals;
  },
  getIsFirstTime(state){
    return state.isFirstTime;
  }
};

export default getters;
